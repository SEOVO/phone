<?php
include 'lib/customer.defines.php';
include 'lib/customer.module.access.php';
include 'lib/customer.smarty.php';
include 'lib/epayment/includes/configure.php';
include 'lib/epayment/includes/html_output.php';
include './lib/epayment/includes/general.php';

if (!has_rights(ACX_ACCESS)) {
    Header("HTTP/1.0 401 Unauthorized");
    Header("Location: PP_error.php?c=accessdenied");
    die();
}

$inst_table = new Table();

$QUERY = "SELECT username, credit, lastname, firstname, address, city, state, country, zipcode, phone, email, fax, lastuse, activated, status, freetimetocall, label, packagetype, billingtype, startday, id_cc_package_offer, cc_card.id, currency,cc_card.useralias,UNIX_TIMESTAMP(cc_card.creationdate) creationdate  FROM cc_card LEFT JOIN cc_tariffgroup ON cc_tariffgroup.id=cc_card.tariff LEFT JOIN cc_package_offer ON cc_package_offer.id=cc_tariffgroup.id_cc_package_offer LEFT JOIN cc_card_group ON cc_card_group.id=cc_card.id_group WHERE username = '" . $_SESSION["pr_login"] . "' AND uipass = '" . $_SESSION["pr_password"] . "'";

$DBHandle = DbConnect();

$customer_res = $inst_table -> SQLExec($DBHandle, $QUERY);

if (!$customer_res || !is_array($customer_res)) {
    echo gettext("Error loading your account information!");
    exit ();
}

$customer_info = $customer_res[0];

if ($customer_info[14] != "1" && $customer_info[14] != "8") {
    Header("HTTP/1.0 401 Unauthorized");
    Header("Location: PP_error.php?c=accessdenied");
    die();
}

$customer = $_SESSION["pr_login"];

getpost_ifset(array('posted', 'Period', 'frommonth', 'fromstatsmonth', 'tomonth', 'tostatsmonth', 'fromday', 'fromstatsday_sday', 'fromstatsmonth_sday', 'today', 'tostatsday_sday', 'tostatsmonth_sday', 'dsttype', 'sourcetype', 'clidtype', 'channel', 'resulttype', 'stitle', 'atmenu', 'current_page', 'order', 'sens', 'dst', 'src', 'clid','subscribe'));

$currencies_list = get_currencies();

$two_currency = false;
if (!isset ($currencies_list[strtoupper($customer_info[22])][2]) || !is_numeric($currencies_list[strtoupper($customer_info[22])][2])) {
    $mycur = 1;
} else {
    $mycur = $currencies_list[strtoupper($customer_info[22])][2];
    $display_currency = strtoupper($customer_info[22]);
    if (strtoupper($customer_info[22]) != strtoupper(BASE_CURRENCY)) {
        $two_currency = true;
    }
}

$credit_cur = $customer_info[1] / $mycur;
$credit_cur = round($credit_cur, 3);
$useralias = $customer_info['useralias'];
$creation_date = $customer_info['creationdate'];
$username = $customer_info['username'];

$smarty->display('main.tpl');
?>

<?php


$user_info = array(
  gettext("PHONE")    => $customer_info[9],
  gettext("ADDRESS")  =>  $customer_info[4],
  gettext("ZIP CODE") => $customer_info[8],
  gettext("CITY")     => $customer_info[5],
  gettext("STATE")    => $customer_info[6],
  gettext("COUNTRY")  => $customer_info[7],
  gettext("FAX") => $customer_info[11]
); 


//funcion observada evaluar con la copia
if($customer_info[15] > 0) {
  $info_extra = $customer_info[15];
  $freetimetocall_used = $A2B->FT2C_used_seconds($DBHandle, $customer_info[21], $customer_info[20], $customer_info[18], $customer_info[19]);

  if (($customer_info[17]==0) || ($customer_info[17]==1)) {
     $ex1 = gettext("PACKAGE MINUTES REMAINING");
     $ex2 =  1;
  } else {
     $ex1 = gettext("PACKAGE MINUTES USED");
     $ex2 = 0;

    }



  
}


 if (has_rights (ACX_PERSONALINFO)) { 
       $edit_info = "A2B_entity_card.php?atmenu=password&form_action=ask-edit&stitle=Personal+Information";
       
    } 

?>




<?php
include 'templates/default/user_info.php';
$smarty->display('footer.tpl');
