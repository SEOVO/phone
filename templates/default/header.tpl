<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<HEAD>
	<link rel="shortcut icon" href="templates/{$SKIN_NAME}/images/a2billing-icon-32x32.ico">
	<title>..:: {$CCMAINTITLE} ::..</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- <link href="templates/{$SKIN_NAME}/css/main.css" rel="stylesheet" type="text/css">
    <link href="templates/{$SKIN_NAME}/css/menu.css" rel="stylesheet" type="text/css"> -->
    
    {if LANGUAGE == "farsi" }
        <link href="templates/{$SKIN_NAME}/css/right2left.css" rel="stylesheet" type="text/css">
    {/if}
    
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" href="templates/{$SKIN_NAME}/css/style-ie.css" />
	<![endif]-->
	<!--<link href="templates/{$SKIN_NAME}/css/style-def.css" rel="stylesheet" type="text/css">-->
	<link href="templates/{$SKIN_NAME}/css/invoice.css" rel="stylesheet" type="text/css">
	<link href="templates/{$SKIN_NAME}/css/receipt.css" rel="stylesheet" type="text/css">
	{if ($popupwindow != 0)}
		<link href="templates/{$SKIN_NAME}/css/popup.css" rel="stylesheet" type="text/css">
 	{/if}
	<script type="text/javascript">	
		var IMAGE_PATH = "templates/{$SKIN_NAME}/images/";
	</script>
	<script type="text/javascript" src="./javascript/jquery/jquery-1.2.6.min.js"></script>
	<script type="text/javascript" src="./javascript/jquery/jquery.debug.js"></script>
	<script type="text/javascript" src="./javascript/jquery/ilogger.js"></script>
	<script type="text/javascript" src="./javascript/jquery/handler_jquery.js"></script>
	<script language="javascript" type="text/javascript" src="./javascript/misc.js"></script>

   
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
       <!-- Font Awesome -->
       <link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
       <!-- Ionicons -->
       <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> 
       <link rel="stylesheet" href="AdminLTE/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> 
       <link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
       <!-- JQVMap -->
       <link rel="stylesheet" href="AdminLTE/plugins/jqvmap/jqvmap.min.css">
       <!-- Theme style -->
       <link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
       <!-- overlayScrollbars -->
       <link rel="stylesheet" href="AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
       <!-- Daterange picker -->
       <link rel="stylesheet" href="AdminLTE/plugins/daterangepicker/daterangepicker.css">
       <!-- summernote -->
       <link rel="stylesheet" href="AdminLTE/plugins/summernote/summernote-bs4.min.css">

</HEAD>
