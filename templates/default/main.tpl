{include file="header.tpl"}
{if ($popupwindow == 0)}
{if ($EXPORT == 0)}

<!-- las clases indicadas en el body es del adminlte -->
<BODY class="hold-transition sidebar-mini">
<!-- la clase wraper es el contenedor paratrabajar con el  adminlte  -->
<div class="wrapper"  id="page-wrap">
        <div id="inside">

<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="userinfo.php" class="nav-link">Inicio</a>
      </li>
    </ul>


    <!-- icono para salir de sesion -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link"  href='logout.php?logout=true'>
          <i class="text-danger fas fa-times-circle"></i>{php} echo gettext("LOGOUT");{/php}
        </a>
      </li>
    </ul>
  </nav>
<!-- Inicio de la barra de navegación  del adminLTE-->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!--  Logo -->
    <a href="#" class="brand-link">
      <img src="AdminLTE/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">WiñaytelFono</span>
    </a>

    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Inicio de un Item -->
          <li class="nav-item ">
            <a href="userinfo.php" class="nav-link ">
             <i class="fas fa-info-circle"></i>
              <p>
                {php} echo gettext("ACCOUNT INFO");{/php}
              </p>
            </a>
          </li>
         <!-- fin de un item -->
          {if $ACXVOICEMAIL>0 }
          <li class="nav-item">
            <a href="A2B_entity_voicemail.php" class="nav-link">
              <i class="nav-icon fas fa-phone-volume"></i>
              <p>
                {php} echo gettext("VOICEMAIL");{/php}
              </p>
            </a>
          </li>
          {/if}
          {if $ACXSIP_IAX>0 }
          <li class="nav-item">
            <a href="A2B_entity_sipiax_info.php" class="nav-link">
              <i class="nav-icon fas fa-phone-volume"></i>
              <p style="font-size: 0.8rem;">
                {php} echo gettext("SIP/IAX INFO");{/php}
              </p>
            </a>
          </li>
          {/if}
          {if $ACXCALL_HISTORY >0 }
          <li class="nav-item">
            <a href="call-history.php" class="nav-link">
              <i class="nav-icon fas fa-phone-volume"></i>
              <p>
                {php} echo gettext("CALL HISTORY");{/php}
              </p>
            </a>
          </li>
          {/if}
          {if $ACXPAYMENT_HISTORY >0 }
          <li class="nav-item">
            <a href="payment-history.php" class="nav-link">
              <i class="nav-icon fas fa-search-dollar"></i>
              <p>
                {php} echo gettext("PAYMENT HISTORY");{/php}
              </p>
            </a>
          </li>
          {/if}
          {if $ACXVOUCHER >0 }
          <li class="nav-item">
            <a href="A2B_entity_voucher.php?form_action=list" class="nav-link">
              <i class="nav-icon fas fa-ticket-alt"></i>
              <p>
                {php} echo gettext("VOUCHERS");{/php}
              </p>
            </a>
          </li>
          {/if}
          {if $ACXINVOICES >0 }  

          <li class="nav-item has-treeview ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file-invoice-dollar"></i>
              <p>
                {php} echo gettext("INVOICES");{/php}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="A2B_entity_receipt.php?section=5" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{php} echo gettext("View Receipts");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="A2B_entity_invoice.php?section=5" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{php} echo gettext("View Invoices");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="A2B_billing_preview.php?section=5" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{php} echo gettext("Preview Next Billing");{/php}</p>
                </a>
              </li>
            </ul>
          </li>

          {/if}

          {if $ACXDID >0 }
        <div class="toggle_menu"><li><a href="A2B_entity_did.php?form_action=list"><strong>{php} echo gettext("DID");{/php}</strong></a></li></div>
        {/if}

        {if $ACXSPEED_DIAL >0 }
        <div class="toggle_menu"><li><a href="A2B_entity_speeddial.php?atmenu=speeddial&stitle=Speed+Dial"><strong>{php} echo gettext("SPEED DIAL");{/php}</strong></a></li></div>
        {/if}

        {if $ACXRATECARD >0 }
        <div class="toggle_menu"><li><a href="A2B_entity_ratecard.php?form_action=list"><strong>{php} echo gettext("RATECARD");{/php}</strong></a></li></div>
        {/if}

        {if $ACXSIMULATOR >0 }
        <div class="toggle_menu"><li><a href="simulator.php"><strong>{php} echo gettext("SIMULATOR");{/php}</strong></a></li></div>
        {/if}

        {if $ACXCALL_BACK >0 }
        <div class="toggle_menu"><li><a href="callback.php"><strong>{php} echo gettext("CALLBACK");{/php}</strong></a></li></div>
        {/if}

        {if $ACXCALLER_ID >0 }
        <div class="toggle_menu"><li><a href="A2B_entity_callerid.php?atmenu=callerid&stitle=CallerID"><strong>{php} echo gettext("ADD CALLER ID");{/php}</strong></a></li></div>
        {/if}

        {if $ACXPASSWORD>0 }
        <div class="toggle_menu"><li><a href="A2B_entity_password.php?atmenu=password&form_action=ask-edit&stitle=Password"><strong>{php} echo gettext("PASSWORD");{/php}</strong></a></li></div>
        {/if}
        {if $ACXSUPPORT >0 }
          <li class="nav-item ">
            <a href="A2B_support.php" class="nav-link ">
              <i class="nav-icon fas fa-question-circle"></i>
              <p>
                {php} echo gettext("SUPPORT");{/php}
              </p>
            </a>
          </li>
        {/if}

        {if $ACXNOTIFICATION >0 }
        <div class="toggle_menu"><li><a href="A2B_notification.php?form_action=ask-edit"><strong>{php} echo gettext("NOTIFICATION");{/php}</strong></a></li></div>
        {/if}
                
        <li class="nav-item has-treeview ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-globe"></i>
              <p>
               {php} echo gettext("IDIOMA");{/php} 
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=english" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/gb.gif" border="0" title="English" alt="English">
                  <p>{php} echo gettext("English");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=spanish" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/es.gif" border="0" title="Spanish" alt="Spanish">
                  <p>{php} echo gettext("Spanish");{/php}</p>
                </a>
              </li>
              <!--
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=french" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/fr.gif" border="0" title="French" alt="French">
                  <p>{php} echo gettext("French");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=german" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/de.gif" border="0" title="German" alt="German">
                  <p>{php} echo gettext("German");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=chinese" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/cn.gif" border="0" title="chinese" alt="chinese">
                  <p>{php} echo gettext("chinese");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=romanian" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/ro.gif" border="0" title="romanian" alt="romanian">
                  <p>{php} echo gettext("romanian");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=italian" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/it.gif" border="0" title="italian" alt="italian">
                  <p>{php} echo gettext("italian");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=brazilian" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/br.gif" border="0" title="brazilian" alt="brazilian">
                  <p>{php} echo gettext("brazilian");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=polish" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/pl.gif" border="0" title="polish" alt="polish">
                  <p>{php} echo gettext("polish");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=russian" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/ru.gif" border="0" title="russian" alt="russian">
                  <p>{php} echo gettext("russian");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=turkish" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/tr.gif" border="0" title="turkish" alt="turkish">
                  <p>{php} echo gettext("turkish");{/php}</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=portuguese" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/pt.gif" border="0" title="portuguese" alt="portuguese">
                  <p>{php} echo gettext("portuguese");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=urdu" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/pk.gif" border="0" title="urdu" alt="urdu">
                  <p>{php} echo gettext("urdu");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=ukrainian" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/ua.gif" border="0" title="ukrainian" alt="ukrainian">
                  <p>{php} echo gettext("ukrainian");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=farsi" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/ir.gif" border="0" title="farsi" alt="farsi">
                  <p>{php} echo gettext("farsi");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=greek" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/gr.gif" border="0" title="greek" alt="greek">
                  <p>{php} echo gettext("greek");{/php}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{$PAGE_SELF}?ui_language=indonesian" class="nav-link">
                  <img class ="nav-icon"  src="templates/{$SKIN_NAME}/images/flags/id.gif" border="0" title="indonesian" alt="indonesian">
                  <p>{php} echo gettext("indonesian");{/php}</p>
                </a>
              </li>
               -->
            </ul>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

</div>

<div id="main-content">
<br/>
{else}
	<div>
{/if}

{else}
	<div>
{/if}

{$MAIN_MSG}
