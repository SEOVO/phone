<div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <h1 class="m-0 text-dark">Historial de Llamadas</h1>
          </div><!-- /.col -->
          <div class="col-sm-9">
              <?=$CC_help_balance_customer?>

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <form METHOD=POST ACTION="<?php echo $PHP_SELF?>?s=1&t=0&order=<?php echo $order?>&sens=<?php echo $sens?>&current_page=<?php echo $current_page?>&terminatecauseid=<?php echo $terminatecauseid?>">
        <INPUT TYPE="hidden" NAME="posted" value=1>
        <INPUT TYPE="hidden" NAME="current_page" value=0>
        <table class="table callhistory_maintable  table-sm" >

            <tr>
                <td align="left" class="bgcolor_002"> &nbsp;
                    <font class="fontstyle_003"><?php echo gettext("DATE");?></b></font>
                </td>
                  <td align="left" class="bgcolor_003">
                    <table  >
                    <tr class="row"><td class="col-sm fontstyle_searchoptions">
                      <input type="checkbox" name="fromday" value="true" <?php  if ($fromday) { ?>checked<?php }?>> <?php echo gettext("FROM");?> :
                    <select name="fromstatsday_sday" class="form_input_select">
                        <?php
                            for ($i=1;$i<=31;$i++) {
                                if ($fromstatsday_sday==sprintf("%02d",$i)) {$selected="selected";} else {$selected="";}
                                echo '<option value="'.sprintf("%02d",$i)."\"$selected>".sprintf("%02d",$i).'</option>';
                            }
                        ?>
                    </select>
                     <select name="fromstatsmonth_sday" class="form_input_select">
                    <?php
                        $year_actual = date("Y");
                        for ($i=$year_actual;$i >= $year_actual-1;$i--) {
                            $monthname = array( gettext("JANUARY"), gettext("FEBRUARY"), gettext("MARCH"), gettext("APRIL"), gettext("MAY"), gettext("JUNE"), gettext("JULY"), gettext("AUGUST"), gettext("SEPTEMBER"), gettext("OCTOBER"), gettext("NOVEMBER"), gettext("DECEMBER"));
                            if ($year_actual==$i) {
                                $monthnumber = date("n")-1; // Month number without lead 0.
                            } else {
                                $monthnumber=11;
                            }
                            for ($j=$monthnumber;$j>=0;$j--) {
                                $month_formated = sprintf("%02d",$j+1);                                $month_formated = sprintf("%02d",$j+1);
                                   if ($fromstatsmonth_sday=="$i-$month_formated") {$selected="selected";} else {$selected="";}
                                echo "<OPTION value=\"$i-$month_formated\" $selected> $monthname[$j]-$i </option>";
                            }
                        }
                    ?>
                    </select>
                    </td><td class="fontstyle_searchoptions">&nbsp;&nbsp;
                    <input type="checkbox" name="today" value="true" <?php  if ($today) { ?>checked<?php }?>> <?php echo gettext("TO");?> :
                    <select name="tostatsday_sday" class="form_input_select">
                    <?php
                        for ($i=1;$i<=31;$i++) {
                            if ($tostatsday_sday==sprintf("%02d",$i)) {$selected="selected";} else {$selected="";}
                            echo '<option value="'.sprintf("%02d",$i)."\"$selected>".sprintf("%02d",$i).'</option>';
                        }
                    ?>
                    </select>
                     <select name="tostatsmonth_sday" class="form_input_select">
                    <?php       $year_actual = date("Y");
                        for ($i=$year_actual;$i >= $year_actual-1;$i--) {
                               $monthname = array( gettext("JANUARY"), gettext("FEBRUARY"), gettext("MARCH"), gettext("APRIL"), gettext("MAY"), gettext("JUNE"), gettext("JULY"), gettext("AUGUST"), gettext("SEPTEMBER"), gettext("OCTOBER"), gettext("NOVEMBER"), gettext("DECEMBER"));
                               if ($year_actual==$i) {
                                    $monthnumber = date("n")-1; // Month number without lead 0.
                               } else {
                                    $monthnumber=11;
                               }
                               for ($j=$monthnumber;$j>=0;$j--) {
                                        $month_formated = sprintf("%02d",$j+1);
                                           if ($tostatsmonth_sday=="$i-$month_formated") {$selected="selected";} else {$selected="";}
                                        echo "<OPTION value=\"$i-$month_formated\" $selected> $monthname[$j]-$i </option>";
                               }
                        }
                    ?> 
                </select>
                    </td></tr></table>
                  </td>
            </tr>
            <tr>
                <td  align="left" class="bgcolor_004">
                    <font class="fontstyle_003">&nbsp;&nbsp;<?php echo gettext("PHONENUMBER");?></font>
                </td>
                <td  align="left" class="bgcolor_005">
                <table  border="0" cellspacing="0" cellpadding="0">
                <tr class="row">
                 <td class="col-sm fontstyle_searchoptions">&nbsp;&nbsp;<INPUT TYPE="text" NAME="phonenumber" value="<?php echo $phonenumber?>" class="form_input_text"></td>
                <td class="col-sm row"> 
                 <div  align="center" class="fontstyle_searchoptions"><input type="radio" NAME="phonenumbertype" value="1" <?php if ((!isset($phonenumbertype))||($phonenumbertype==1)) {?>checked<?php }?>><?php echo gettext("Exact");?></div>
                <div  align="center" class="fontstyle_searchoptions"><input type="radio" NAME="phonenumbertype" value="2" <?php if ($phonenumbertype==2) {?>checked<?php }?>><?php echo gettext("Begins with")?></div>
                <div align="center" class="fontstyle_searchoptions"><input type="radio" NAME="phonenumbertype" value="3" <?php if ($phonenumbertype==3) {?>checked<?php }?>><?php echo gettext("Contains");?></div>
                <div  align="center" class="fontstyle_searchoptions"><input type="radio" NAME="phonenumbertype" value="4" <?php if ($phonenumbertype==4) {?>checked<?php }?>><?php echo gettext("Ends with");?></div>
                </td>
                </tr></table></td>
            </tr>
            <!-- Select Calltype: -->
            <tr>
              <td class="bgcolor_002" align="left" ><font class="fontstyle_003">&nbsp;&nbsp;<?php echo gettext("CALL TYPE"); ?></font></td>
              <td class="bgcolor_003" align="center">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                    <td  class="fontstyle_searchoptions">
                    <select NAME="choose_calltype" size="1" class="form_input_select" >
                        <option value='-1' <?php if (($choose_calltype==-1) || (!isset($choose_calltype))) {?>selected<?php } ?>><?php echo gettext('ALL CALLS') ?>
                                </option>
                            <?php
                                foreach ($list_calltype as $key => $cur_value) {
                            ?>
                                <option value='<?php echo $cur_value[1] ?>' <?php if ($choose_calltype==$cur_value[1]) {?>selected<?php } ?>><?php echo gettext($cur_value[0]) ?>
                                </option>
                            <?php       } ?>
                        </select>
                    </td>
                </tr>
                </table>
               </td>
            </tr>
            <!-- Select Option : to show just the Answered Calls or all calls, Result type, currencies... -->
            <tr>
              <td class="bgcolor_002" align="left" ><font class="fontstyle_003">&nbsp;&nbsp;<?php echo gettext("OPTIONS"); ?></font></td>
              <td class="bgcolor_003" align="center">
              <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20%"  class="fontstyle_searchoptions">
                        <?php echo gettext("SHOW");?> :
                   </td>
                   <td width="80%"  class="fontstyle_searchoptions">
                 <?php echo gettext("Answered Calls"); ?>
                  <input name="terminatecauseid" type="radio" value="ANSWER" <?php if ((!isset($terminatecauseid))||($terminatecauseid=="ANSWER")) {?>checked<?php }?> />
                  <?php echo gettext("All Calls"); ?>

                   <input name="terminatecauseid" type="radio" value="ALL" <?php if ($terminatecauseid=="ALL") {?>checked<?php }?>/>
                    </td>
                </tr>
                <tr class="bgcolor_005">
                    <td  class="fontstyle_searchoptions">
                        <?php echo gettext("RESULT");?> :
                   </td>
                   <td  class="fontstyle_searchoptions">
                    <?php echo gettext("Minutes");?><input type="radio" NAME="resulttype" value="min" <?php if ((!isset($resulttype))||($resulttype=="min")){?>checked<?php }?>> - <?php echo gettext("Seconds");?> <input type="radio" NAME="resulttype" value="sec" <?php if ($resulttype=="sec") {?>checked<?php }?>>
                    </td>
                </tr>
                <tr>
                    <td  class="fontstyle_searchoptions">
                        <?php echo gettext("CURRENCY");?> :
                    </td>
                    <td  class="fontstyle_searchoptions">
                    <select NAME="choose_currency"  class="form-control" >
                            <?php
                                $currencies_list = get_currencies();
                                foreach ($currencies_list as $key => $cur_value) {
                            ?>
                                <option value='<?php echo $key ?>' <?php if (($choose_currency==$key) || (!isset($choose_currency) && $key==strtoupper(BASE_CURRENCY))) {?>selected<?php } ?>><?php echo $cur_value[1].' ('.$cur_value[2].')' ?>
                                </option>
                            <?php       } ?>
                        </select>
                    </td>
                </tr>
                </table>
              </td>
              </tr>
            <!-- Select Option : to show just the Answered Calls or all calls, Result type, currencies... -->

            <tr>
                <td class="bgcolor_004" align="left" > </td>
                <td class="bgcolor_005" align="center" >
                    <input class="form_input_button" value=" <?php echo gettext("Search");?> " type="submit">
                  </td>
            </tr>       
        </table>
    </form>

    </section>

    <section class="content">
       <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab"><?php echo gettext("Number of Calls");?> : <?php  if (is_array($list) && count($list)>0) { echo $nb_record; } else {echo "0";}?></a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab"><?php echo gettext("CALLING CARD MINUTES");?></a></li>
                  
                </ul>
              </div><!-- /.card-header -->
              <div>
                <div class="tab-content">
                  <div class="tab-pane active" id="activity">
                    
                    <?php include 'templates/default/call-history_list.php'; ?>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                     <?php include 'templates/default/call-history_list2.php'; ?>
                    
                  </div>
                  <!-- /.tab-pane -->

                  
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>   
    </section>
</div>
