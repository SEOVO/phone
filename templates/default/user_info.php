  <!-- clase que indica el cuerpo del AdminLTE -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Información Principal</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Información de la cuenta</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="https://asoclinic.com/wp-content/uploads/2018/02/placeholder-face-big.png"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?php echo $customer_info[2].' '.$customer_info[3]; ?></h3>

                <p class="text-muted text-center"><?php echo $customer_info[10]; ?></p>

                <ul class="list-group list-group-unbordered mb-3">
                 <?php foreach ($user_info as $k => $i) {   ?>
                  <li class="list-group-item">
                    <b><?=$k ?></b> <a class="float-right"><?=$i  ?></a>
                  </li>
                 <?php } ?>
                </ul>
                <?php if (!empty($edit_info)) { ?>
        <a class="btn btn-primary btn-block"  href="<?=$edit_info ?>"><?php echo gettext("EDIT PERSONAL INFORMATION");?></a>
        <?php } ?>

              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item mr-4">
                    <div class="">
                      <!-- small box -->
                      <div class="small-box bg-info">
                       <div class="inner">
                         <h3><?php echo $customer_info[0]; ?></h3>
                         <p class="text-white"><?php echo gettext("CARD NUMBER");?></p>
                       </div>
                      <div class="icon">
                          <i class="fas fa-credit-card"></i>
                      </div>
                    </div>
                    </div>
                  </li>
                  <li class="nav-item mr-4">
                    <div class="">
                      <!-- small box -->
                      <div class="small-box bg-danger">
                       <div class="inner">
                         <h3><?php echo $credit_cur.' '.$customer_info[22]; ?></h3>
                         <p class="text-white"><?php echo gettext("BALANCE REMAINING");?></p>
                       </div>
                      <div class="icon">
                          <i class="fas fa-comment-dollar"></i>
                      </div>
                    </div> 
                    </div>
                  </li>
                <?php if(!empty($info_extra)){  ?>
                  <li class="nav-item mr-4">
                    <div class="">
                      <!-- small box -->
                      <div class="small-box bg-success">
                       <div class="inner">
                         <h3><?php echo (empty($customer_info[16]))?'0':$customer_info[16] ?></h3>
                         <p class="text-white"><?php echo gettext("CALLING PACKAGE");?></p>
                       </div>
                      <div class="icon">
                          <i class="fas fa-phone-volume"></i>
                      </div>
                    </div> 
                    </div>
                  </li>
                  <li class="nav-item mr-4">
                    <div class="">
                      <!-- small box -->
                      <div class="small-box bg-info">
                       <div class="inner">
                         <h3><?=$ex1 ?> </h3>
                         <p class="text-white">
                           <?php 

                               if($ex2==1){
                                   printf ("%d:%02d of %d:%02d",
                  intval(($customer_info[15]-$freetimetocall_used) / 60),($customer_info[15]-$freetimetocall_used) % 60,intval($customer_info[15]/60),$customer_info[15] % 60);
                                }else{
                                  printf ("%d:%02d",intval($freetimetocall_used / 60),$freetimetocall_used % 60);
                                }
                            ?>
                         </p>
                       </div>
                      <div class="icon">
                          <i class="fas fa-phone-volume"></i>
                      </div>
                    </div> 
                    </div>
                  </li> 
                <?php }  ?>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                     <?php  include 'suscribirse.php'; ?>
                    </div>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
