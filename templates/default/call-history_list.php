<center> <?php echo gettext("Number of Calls");?> : <?php  if (is_array($list) && count($list)>0) { echo $nb_record; } else {echo "0";}?> </center>
<div class="card-body table-responsive p-0">
<table width="<?php echo $FG_HTML_TABLE_WIDTH?>" border="0" align="center" cellpadding="0" cellspacing="0">
        <TR bgcolor="#ffffff">
          <TD class="callhistory_td11">
            <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                <TR>
                  <TD><SPAN style="COLOR: #ffffff; FONT-SIZE: 11px"><B><?php echo $FG_HTML_TABLE_TITLE?></B></SPAN></TD>
                </TR>
            </TABLE></TD>
        </TR>
        <TR>
          <TD> <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                <TR class="bgcolor_008">
                  <TD width="<?php echo $FG_ACTION_SIZE_COLUMN?>" align="center" class="tableBodyRight" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"></TD>

                  <?php
                      if (is_array($list) && count($list)>0) {

                      for ($i=0;$i<$FG_NB_TABLE_COL;$i++) {
                    ?>
                      <TD width="<?php echo $FG_TABLE_COL[$i][2]?>" align=middle class="tableBody" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
                        <center><strong>
                        <?php  if (strtoupper($FG_TABLE_COL[$i][4])=="SORT") {?>
                        <a href="<?php  echo $PHP_SELF."?customer=$customer&s=1&t=0&stitle=$stitle&atmenu=$atmenu&current_page=$current_page&order=".$FG_TABLE_COL[$i][1]."&sens="; if ($sens=="ASC") {echo"DESC";} else {echo"ASC";}
                        echo "&posted=$posted&Period=$Period&frommonth=$frommonth&fromstatsmonth=$fromstatsmonth&tomonth=$tomonth&tostatsmonth=$tostatsmonth&fromday=$fromday&fromstatsday_sday=$fromstatsday_sday&fromstatsmonth_sday=$fromstatsmonth_sday&today=$today&tostatsday_sday=$tostatsday_sday&tostatsmonth_sday=$tostatsmonth_sday&phonenumbertype=$phonenumbertype&sourcetype=$sourcetype&clidtype=$clidtype&channel=$channel&resulttype=$resulttype&phonenumber=$phonenumber&src=$src&clid=$clid&terminatecauseid=$terminatecauseid&choose_calltype=$choose_calltype";?>">
                        <span class="liens"><?php  } ?>
                        <?php echo $FG_TABLE_COL[$i][0]?>
                        <?php if ($order==$FG_TABLE_COL[$i][1] && $sens=="ASC") {?>
                        &nbsp;<img src="<?php echo Images_Path_Main ?>/icon_up_12x12.GIF" width="12" height="12" border="0">
                        <?php } elseif ($order==$FG_TABLE_COL[$i][1] && $sens=="DESC") {?>
                        &nbsp;<img src="<?php echo Images_Path_Main ?>/icon_down_12x12.GIF" width="12" height="12" border="0">
                        <?php }?>
                        <?php  if (strtoupper($FG_TABLE_COL[$i][4])=="SORT") {?>
                        </span></a>
                        <?php }?>
                        </strong></center></TD>
                   <?php } ?>

                </TR>
                <TR>
                  <TD bgColor="#e1e1e1" colSpan=<?php echo $FG_TOTAL_TABLE_COL?> height="1">
                </TR>
                <?php
                       $ligne_number=0;
                       foreach ($list as $recordset) {
                         $ligne_number++;
                         $recordset[0] = display_GMT($recordset[0], $_SESSION["gmtoffset"], 1);
                ?>

                        <TR bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$ligne_number%2]?>"  onMouseOver="bgColor='#C4FFD7'" onMouseOut="bgColor='<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$ligne_number%2]?>'">
                        <TD align="<?php echo $FG_TABLE_COL[$i][3]?>" class="tableBody"><?php  echo $ligne_number+$current_page*$FG_LIMITE_DISPLAY.".&nbsp;"; ?></TD>

                          <?php for ($i=0;$i<$FG_NB_TABLE_COL;$i++) { ?>
                        <?php
                            if ($FG_TABLE_COL[$i][6]=="lie") {
                                    $instance_sub_table = new Table($FG_TABLE_COL[$i][7], $FG_TABLE_COL[$i][8]);
                                    $sub_clause = str_replace("%id", $recordset[$i], $FG_TABLE_COL[$i][9]);
                                    $select_list = $instance_sub_table -> Get_list ($DBHandle, $sub_clause, null, null, null, null, null, null);

                                    $field_list_sun = preg_split('/,/',$FG_TABLE_COL[$i][8]);
                                    $record_display = $FG_TABLE_COL[$i][10];

                                    for ($l=1;$l<=count($field_list_sun);$l++) {
                                        $record_display = str_replace("%$l", $select_list[0][$l-1], $record_display);
                                    }

                            } elseif ($FG_TABLE_COL[$i][6]=="list") {
                                    $select_list = $FG_TABLE_COL[$i][7];
                                    $record_display = $select_list[$recordset[$i]][0];

                            } else {
                                    $record_display = $recordset[$i];
                            }

                            if ( is_numeric($FG_TABLE_COL[$i][5]) && (strlen($record_display) > $FG_TABLE_COL[$i][5]) ) {
                                $record_display = substr($record_display, 0, $FG_TABLE_COL[$i][5]-3)."";
                            }

                          ?>
                          <TD vAlign=top align="<?php echo $FG_TABLE_COL[$i][3]?>" class=tableBody><?php
                          if (isset ($FG_TABLE_COL[$i][11]) && strlen($FG_TABLE_COL[$i][11])>1) {
                             call_user_func($FG_TABLE_COL[$i][11], $record_display);
                         } else {
                             echo stripslashes($record_display);
                         }
                         ?></TD>
                          <?php  } ?>

                    </TR>
                <?php
                     }//foreach ($list as $recordset)
                     if ($ligne_number < $FG_LIMITE_DISPLAY)  $ligne_number_end=$ligne_number +2;
                     while ($ligne_number < $ligne_number_end) {
                         $ligne_number++;
                ?>
                    <TR bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$ligne_number%2]?>">
                          <?php for ($i=0;$i<$FG_NB_TABLE_COL;$i++) {
                          ?>
                          <TD vAlign=top class=tableBody>&nbsp;</TD>
                          <?php  } ?>
                          <TD align="center" vAlign=top class=tableBodyRight>&nbsp;</TD>
                    </TR>

                <?php
                     } //END_WHILE

                  } else {
                          echo gettext("No data found !!!");
                  }//end_if
                 ?>
            </TABLE></td>
        </tr>
        <TR bgcolor="#ffffff">
          <TD bgColor=#ADBEDE height=16 style="PADDING-LEFT: 5px; PADDING-RIGHT: 3px">
            <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                <TR>
                  <TD align="right"><SPAN style="COLOR: #ffffff; FONT-SIZE: 11px"><B>
                    <?php if ($current_page>0) {?>
                    <img src="<?php echo Images_Path_Main ?>/fleche-g.gif" width="5" height="10"> <a href="<?php echo $PHP_SELF?>?s=1&t=0&order=<?php echo $order?>&sens=<?php echo $sens?>&current_page=<?php  echo ($current_page-1)?><?php  if (!is_null($letter) && ($letter!="")) { echo "&letter=$letter";}
                    echo "&customer=$customer&posted=$posted&Period=$Period&frommonth=$frommonth&fromstatsmonth=$fromstatsmonth&tomonth=$tomonth&tostatsmonth=$tostatsmonth&fromday=$fromday&fromstatsday_sday=$fromstatsday_sday&fromstatsmonth_sday=$fromstatsmonth_sday&today=$today&tostatsday_sday=$tostatsday_sday&tostatsmonth_sday=$tostatsmonth_sday&phonenumbertype=$phonenumbertype&sourcetype=$sourcetype&clidtype=$clidtype&channel=$channel&resulttype=$resulttype&phonenumber=$phonenumber&src=$src&clid=$clid&terminatecauseid=$terminatecauseid&choose_calltype=$choose_calltype";?>">
                    <?php echo gettext("PREVIOUS");?> </a> -
                    <?php }?>
                    <?php echo ($current_page+1);?> / <?php  echo $nb_record_max;?>
                    <?php if ($current_page<$nb_record_max-1) {?>
                    - <a href="<?php echo $PHP_SELF?>?s=1&t=0&order=<?php echo $order?>&sens=<?php echo $sens?>&current_page=<?php  echo ($current_page+1)?><?php  if (!is_null($letter) && ($letter!="")) { echo "&letter=$letter";}
                    echo "&customer=$customer&posted=$posted&Period=$Period&frommonth=$frommonth&fromstatsmonth=$fromstatsmonth&tomonth=$tomonth&tostatsmonth=$tostatsmonth&fromday=$fromday&fromstatsday_sday=$fromstatsday_sday&fromstatsmonth_sday=$fromstatsmonth_sday&today=$today&tostatsday_sday=$tostatsday_sday&tostatsmonth_sday=$tostatsmonth_sday&phonenumbertype=$phonenumbertype&sourcetype=$sourcetype&clidtype=$clidtype&channel=$channel&resulttype=$resulttype&phonenumber=$phonenumber&src=$src&clid=$clid&terminatecauseid=$terminatecauseid&choose_calltype=$choose_calltype";?>">
                    <?php echo gettext("NEXT");?> </a> <img src="<?php echo Images_Path_Main ?>/fleche-d.gif" width="5" height="10">
                    </B></SPAN>
                    <?php }?>
                  </TD>
            </TABLE></TD>
        </TR>
      </table>
</div>
