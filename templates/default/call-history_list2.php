<!-- ** ** ** ** ** Part to display the GRAPHIC ** ** ** ** ** -->
<br>

<?php

if (is_array($list_total_day) && count($list_total_day)>0) {

$mmax=0;
$totalcall==0;
$totalminutes=0;
foreach ($list_total_day as $data) {
    if ($mmax < $data[1]) $mmax=$data[1];
    $totalcall+=$data[3];
    $totalminutes+=$data[1];
    $totalcost+=$data[2];
}

?>


<div class="card-body table-responsive p-0">
<table class="table">
<tr><td >
    <table border="0" cellspacing="1" cellpadding="2" width="100%">
    <thead class="thead-light">
    <tr  >
        <th align="center" class="callhistory_td2"><?php echo gettext("SUMMARY");?></th>
        <th class="callhistory_td3" align="center" colspan="5"><?php echo gettext("CALLING CARD MINUTES");?></th>
    </tr>
    </thead>
    <tr>
        <td align="center" class="callhistory_td3"><?php echo gettext("DATE");?></td>
        <td align="center" class="callhistory_td2"><?php echo gettext("DURATION");?></td>
        <td align="center" class="callhistory_td2"><?php echo gettext("GRAPHIC");?></td>
        <td align="center" class="callhistory_td2"><?php echo gettext("CALLS");?></td>
        <td align="center" class="callhistory_td2"><acronym title="<?php echo gettext("AVERAGE LENGTH OF CALL");?>"><?php echo gettext("ALOC");?></acronym></font></td>
        <td align="center" class="callhistory_td2"><?php echo gettext("TOTAL COST");?></td>

        <!-- LOOP -->
    <?php
        $i=0;
        foreach ($list_total_day as $data) {
            $i=($i+1)%2;
            $tmc = $data[1]/$data[3];

            if ((!isset($resulttype)) || ($resulttype=="min")) {
                $tmc = sprintf("%02d",intval($tmc/60)).":".sprintf("%02d",intval($tmc%60));
            } else {
                $tmc =intval($tmc);
            }

            if ((!isset($resulttype)) || ($resulttype=="min")) {
                $minutes = sprintf("%02d",intval($data[1]/60)).":".sprintf("%02d",intval($data[1]%60));
            } else {
                $minutes = $data[1];
            }
            if ($mmax>0) 	$widthbar= intval(($data[1]/$mmax)*200);

        ?>
            </tr><tr>
            <td align="right" class="sidenav" nowrap="nowrap"><font class="callhistory_td5"><?php echo $data[0]?></font></td>
            <td bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$i]?>" align="right" nowrap="nowrap" class="fontstyle_001"><?php echo $minutes?> </td>
            <td bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$i]?>" align="left" nowrap="nowrap" width="<?php echo $widthbar+60?>">
                <table cellspacing="0" cellpadding="0"><tr>
                    <td bgcolor="#e22424"><img src="<?php echo Images_Path_Main ?>/spacer.gif" width="<?php echo $widthbar?>" height="6"></td>
                </tr></table>
            </td>
            <td bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$i]?>" align="right" nowrap="nowrap" class="fontstyle_001"><?php echo $data[3]?></td>
            <td bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$i]?>" align="right" nowrap="nowrap" class="fontstyle_001" ><?php echo $tmc?> </td>
            <td bgcolor="<?php echo $FG_TABLE_ALTERNATE_ROW_COLOR[$i]?>" align="right" nowrap="nowrap" class="fontstyle_001"><?php  display_2bill($data[2]) ?></td>
         <?php
         }

        if ((!isset($resulttype)) || ($resulttype=="min")) {
            $total_tmc = sprintf("%02d",intval(($totalminutes/$totalcall)/60)).":".sprintf("%02d",intval(($totalminutes/$totalcall)%60));
            $totalminutes = sprintf("%02d",intval($totalminutes/60)).":".sprintf("%02d",intval($totalminutes%60));
        } else {
            $total_tmc = intval($totalminutes/$totalcall);
        }

     ?>
    </tr>

    <!-- TOTAL -->
    <tr class="callhistory_td2">
        <td align="right" nowrap="nowrap" class="callhistory_td4"><?php echo gettext("TOTAL");?></td>
        <td align="center" nowrap="nowrap" colspan="2" class="callhistory_td4"><?php echo $totalminutes?> </td>
        <td align="center" nowrap="nowrap" class="callhistory_td4"><?php echo $totalcall?></td>
        <td align="center" nowrap="nowrap" class="callhistory_td4"><?php echo $total_tmc?></td>
        <td align="center" nowrap="nowrap" class="callhistory_td4"><?php  display_2bill($totalcost) ?></td>
    </tr>

    </table>

</td></tr></table>
</div>
<?php  } else { ?>
    <center><h3><?php echo gettext("No calls in your selection");?>.</h3></center>
<?php  } ?>


