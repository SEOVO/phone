{php} header('X-Frame-Options: SAMEORIGIN'); {/php}
<HTML>
<HEAD>
	<link rel="shortcut icon" href="templates/{$SKIN_NAME}/images/a2billing-icon-32x32.ico">
	<title>..:: {$CCMAINTITLE} ::..</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		{if ($CSS_NAME!="" && $CSS_NAME!="default")}
			   <link href="templates/default/css/{$CSS_NAME}.css" rel="stylesheet" type="text/css">
		{else}
                           <!-- Estilos de Admin LTE -->
                            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
                            <!-- Font Awesome -->
                            <link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
                            <!-- icheck bootstrap -->
                            <link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
                            <!-- Theme style -->
                            <link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
		{/if}
        <!-- jQuery -->
<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="AdminLTE/dist/js/adminlte.min.js"></script>
 
</HEAD>

<BODY class="hold-transition login-page" style="    background-image: linear-gradient(rgba(0, 0, 0, 0.51),rgba(0, 0, 0, 0.64) ), url(https://destinonegocio.com/wp-content/uploads/2015/06/ico-destinonegocio-voip-istock-getty-images.jpg);
    background-repeat: no-repeat;
    background-size: cover;
">
{literal}
<script LANGUAGE="JavaScript">
<!--
	function test()
	{
		if(document.form.pr_login.value=="" || document.form.pr_password.value=="") {
			alert("You must enter an user and a password!" + document.form.pr_password.value);
			return false;
		} else {
			return true;
		}
	}
-->
</script>

{/literal}

<!-- Inicio nuevo codigo form ADMin LTE-->
       <div class="login-box">
  <div class="login-logo">
    <a href="#" class="text-white"><b>{php} echo gettext("BIENVENIDO A WINAYTELPHONE");{/php}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form  name="form" method="POST" action="userinfo.php" onsubmit="return test()">
        <input type="hidden" name="done" value="submit_log">
        <div class="input-group mb-3">
          <input  name="pr_login" size="15" value="{$username}"  type="text" class="form-control" placeholder="{php} echo gettext("User");{/php}">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input   name="pr_password" size="15" value="{$password}" type="password" class="form-control" placeholder="{php} echo gettext("Password");{/php}">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <select name="ui_language"  id="ui_language" class="form-control">
                        <option style="background-image:url(templates/{$SKIN_NAME}/images/flags/es.gif);" value="spanish" >Spanish</option>
                </select>
        </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div style="color:#BC2222;font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:bold;padding-left:10px;" >
        {if ($error == 1)}
                {php} echo gettext("AUTHENTICATION REFUSED : please check your user/password!");{/php}
    {elseif ($error==2)}
                {php} echo gettext("INACTIVE ACCOUNT : Your account need to be activated!");{/php}
    {elseif ($error==3)}
                {php} echo gettext("BLOCKED ACCOUNT : Please contact the administrator!");{/php}
    {elseif ($error==4)}
                {php} echo gettext("NEW ACCOUNT : Your account has not been validate yet!");{/php}
    {/if}
    </div>
      <p class="mb-1">
        <a href="forgotpassword.php">{php} echo gettext("Forgot your password ?");{/php}</a>
      </p>
      <p class="mb-0">
        <a href="signup.php" class="text-center">{php} echo gettext("To sign up");{/php}</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>

<!-- fin nuevo codigo Admin LTE-->
{literal}
<script LANGUAGE="JavaScript">
	//document.form.pr_login.focus();
        $("#ui_language").change(function () {
          self.location.href= "index.php?ui_language="+$("#ui_language option:selected").val();
        });
</script>
<!-- jQuery -->
<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="AdminLTE/dist/js/adminlte.min.js"></script>
{/literal}
