 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php echo $CC_help_support;
           if ($form_action == "list") {
            $HD_Form -> create_toppage ("ask-add");

          ?>

        <center><font class="error_message"><?php echo gettext("Create support ticket"); ?></font></center>
        <table class='table' >
        <form name="theForm" action="<?php  $_SERVER["PHP_SELF"]?>">

        <tr class="bgcolor_001">
        <td align="left" valign="bottom">
        <font class="fontstyle_002"><?php echo gettext("Title");?> :</font>
        </td>
        <td>
            <input class="form-control" name="title"  />
        </td>
        </tr>
        <tr>
         <td>
             <font class="fontstyle_002"><?php echo gettext("Priority");?> :</font>
         </td>
         <td>
               <select NAME="priority" class="form-control">
                <option class=input value='0' ><?php echo gettext("NONE");?> </option>
                <option class=input value='1' ><?php echo gettext("LOW");?> </option>
                <option class=input value='2' ><?php echo gettext("MEDIUM");?> </option>
                <option class=input value='3' ><?php echo gettext("HIGH");?> </option>
            </select>
         </td>
        </tr>
          <tr class="bgcolor_001">
         <td>
             <font class="fontstyle_002"><?php echo gettext("Component");?> :</font>
         </td>
         <td>
         <select NAME="component" class="form_input_select">
             <?php
                     $DBHandle  = DbConnect();
                    $instance_sub_table = new Table("cc_support_component", "*");
                 $QUERY = " activated = 1 AND (type_user = 0 OR type_user = 2)";
                 $return = null;
                 $return = $instance_sub_table -> Get_list($DBHandle, $QUERY, 0);
                     foreach ($return as $value) {
                        echo	'<option class=input value=" '. $value["id"].'"  > ' . $value["name"]. '  </option>' ;
                     }
            ?>
                </select>

         </td>
        </tr>
        <tr>
        <td align="left" valign="top">
                <font class="fontstyle_002"><?php echo gettext("Description");?> :</font>
            </td>
            <td>
                 <textarea class="form-control" name="description" ></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" valign="middle">
                        <input class="form_input_button"  value="<?php echo gettext("CREATE");?>"  type="submit">
        </td>
        </tr>
    </form>
      </table>
      <center><font class="error_message"><?php if (isset($update_msg) && strlen($update_msg)>0) echo $update_msg; ?></font></center>
       <?php } 
       // #### TOP SECTION PAGE
$HD_Form -> create_toppage ($form_action);

$HD_Form -> create_form ($form_action, $list, $id=null) ;

     ?>
    </section>

</div>
