<?php


include './lib/customer.defines.php';
include './lib/customer.module.access.php';
include './lib/Form/Class.FormHandler.inc.php';
include './form_data/FG_var_ticket.inc';
include './lib/customer.smarty.php';

if (!has_rights(ACX_SUPPORT)) {
    Header("HTTP/1.0 401 Unauthorized");
    Header("Location: PP_error.php?c=accessdenied");
    die();
}

getpost_ifset(array (
    'title',
    'description',
    'priority',
    'component'
));

$HD_Form->setDBHandler(DbConnect());
$HD_Form->init();

// ADD TICKET
if ((strlen($description) > 0 || strlen($title) > 0) && is_numeric($priority) && is_numeric($component)) {

    $fields = "creator,title, description, id_component, priority, viewed_cust";
    $ticket_table = new Table('cc_ticket', $fields);
    $values = "'" . $_SESSION["card_id"] . "', '" . $title . "', '" . $description . "', '" . $component . "', '" . $priority . "' ,'0'";
    $id_ticket = $ticket_table ->Add_table($HD_Form -> DBHandle, $values, null, null, "id");
    NotificationsDAO::AddNotification("ticket_added_cust", Notification::$LOW, Notification::$CUST, $_SESSION['card_id'], Notification::$LINK_TICKET_CUST, $id_ticket);
    $table_card =new Table("cc_card", "firstname, lastname, language, email");
    $card_clause = "id = ".$_SESSION["card_id"];
    $result=$table_card -> Get_list($HD_Form -> DBHandle, $card_clause);
    $owner = $_SESSION["pr_login"]." (".$result[0]['firstname']." ".$result[0]['lastname'].")";

    try {
        $mail = new Mail(Mail::$TYPE_TICKET_NEW, null, $result[0]['language']);
        $mail->replaceInEmail(Mail::$TICKET_OWNER_KEY, $owner);
        $mail->replaceInEmail(Mail::$TICKET_NUMBER_KEY, $id_ticket);
        $mail->replaceInEmail(Mail::$TICKET_DESCRIPTION_KEY, $description);
        $mail->replaceInEmail(Mail::$TICKET_PRIORITY_KEY, Ticket::DisplayPriority($priority));
        $mail->replaceInEmail(Mail::$TICKET_STATUS_KEY,"NEW");
        $mail->replaceInEmail(Mail::$TICKET_TITLE_KEY, $title);
        $mail->send($result[0]['email']);
    } catch (A2bMailException $e) {
        $error_msg = $e->getMessage();
    }
    $component_table = new Table('cc_support_component LEFT JOIN cc_support ON id_support = cc_support.id', "email");
    $component_clause = "cc_support_component.id = ".$component;
    $result= $component_table -> Get_list($HD_Form -> DBHandle, $component_clause);

    try {
        $mail = new Mail(Mail::$TYPE_TICKET_NEW, null, $result[0]['language']);
        $mail->replaceInEmail(Mail::$TICKET_OWNER_KEY, $owner);
        $mail->replaceInEmail(Mail::$TICKET_NUMBER_KEY, $id_ticket);
        $mail->replaceInEmail(Mail::$TICKET_DESCRIPTION_KEY, $description);
        $mail->replaceInEmail(Mail::$TICKET_PRIORITY_KEY, Ticket::DisplayPriority($priority));
        $mail->replaceInEmail(Mail::$TICKET_STATUS_KEY,"NEW");
        $mail->replaceInEmail(Mail::$TICKET_TITLE_KEY, $title);
        $mail->send($result[0]['email']);
    } catch (A2bMailException $e) {
        $error_msg = $e->getMessage();
    }
    $update_msg = gettext("Ticket added successfully");

} elseif ((strlen($description) + strlen($title) == 0)) {
    $update_msg = gettext("Please complete the title and description portions of the form.");
} else {
    $update_msg = gettext("Sorry, There was a problem creating your ticket.");
}

if ($id != "" || !is_null($id)) {
    $HD_Form->FG_EDITION_CLAUSE = str_replace("%id", "$id", $HD_Form->FG_EDITION_CLAUSE);
}

if (!isset ($form_action))
    $form_action = "list"; //ask-add
if (!isset ($action))
    $action = $form_action;

$list = $HD_Form->perform_action($form_action);

// #### HEADER SECTION
$smarty->display('main.tpl');

// #### HELP SECTION
//echo $CC_help_support;
include 'templates/default/support.php';

?>
<?php
// #### FOOTER SECTION
$smarty->display('footer.tpl');
