# MANUAL DEL DESARROLLADOR
## Rutas 
![rutas_acceso](https://bn1301files.storage.live.com/y4mjAGiiWpr_EDso_5G7Z2MGfDU7fPQ83fjQyJKlEjyWkPOPOnviFYVZnDdaRwu-1jbpiS9ZgHniYL2xA6C9tlZnXOeH7c_LQtK-G7nyTFJVv3KQR0h4X9JHh3-k7a8cb5WNwwf8nGWvy50KHmBQmQgP5-xxpS4_T25KLrYYRfId-F6x5B4ZgnJy2Uoyz8C5osa6BjrIpbVcCCF0I8c_OiMdw/rutas_centos.png?psid=1&width=884&height=146)
### La imagen muestra las rutas para acceder a los directorios , dentro de var/www/html se encuentra los *accesos  directos*  

# AdminLTE 3.0.5
## encontramos dentro de customer la carpeta ** AdminLTE ** la cual es un template que puedes descargar de su pagina : https://adminlte.io/, basado en Boostrap (Responsive) la cual tiene la siguiente estructura:
![adminlte](https://fojuzq.bn.files.1drv.com/y4mvjj2G-DlQK6evpioxotS8nKEx6oHC98xsngM4PUWt7qB6yToSK8l6Gn1bd7aSd4WL7ofJa2MEAFd2oTlz2opPwNWsOHJFEYVJBLbr5vbIkvuI6O6X_vSE5ge7y2U5Bv1YO9BNNR2koCtEceQNo65ZUZERhetGw_sJyE1aT1PnNrCWIKrewNWY51Inp2QPjh8c6yX5NoA6keZBMbAz19FmA/adminlte.png?psid=1)
## aqui tenemos todos los archivos necesarios que deberemos referenciar a las paginas que queremos usar , te puedes guiar con los ejemplos que te ofrece, acontinuación la estructura basica que se implemento para usarlo

# HEADER Y BARRA DE NAVEGACIÓN
## se encuentran en templates/default/main.tpl
  -IMPORTAR CSS DE AdminLTE
  ### al inicio del archivo importamos header.tpl
  ```tlp
    include file="header.tpl"}
    {if ($popupwindow == 0)}
    {if ($EXPORT == 0)}
  ```  
  ### en el archivo header.tlp haremos referencia a todos los archivos css y javascript  que necesitemos
  ```html
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <HEAD>
           <link rel="shortcut icon" href="templates/{$SKIN_NAME}/images/a2billing-icon-32x32.ico">
           <title>..:: {$CCMAINTITLE} ::..</title>
           <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           ...
            <!-- Google Font: Source Sans Pro -->
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
            <!-- Font Awesome -->
            <link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
            ...
            <link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
            <!-- overlayScrollbars -->
            <link rel="stylesheet" href="AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
            <!-- Daterange picker -->
            <link rel="stylesheet" href="AdminLTE/plugins/daterangepicker/daterangepicker.css">
            <!-- summernote -->
            <link rel="stylesheet" href="AdminLTE/plugins/summernote/summernote-bs4.min.css">
        </HEAD>
  ```
  ## El Header de la pagina
  ![header](https://gypn4q.bn.files.1drv.com/y4mW9mJHmTwmn3fDFPoPYWmLoRPZ4jOyoX5mhGeBLOmDYzu0qmZvFV7RrYYzRYGjcVsWOcs9PSssbIQV6FrCdOYKib1UFEiAKYKcbMTBGcHmqxzB4aqxEIpwuFfGP5sn47i1LNdrS19vmG-qspjosUtJhlSqsYO-R7K1Ou2wKEIeZaUK1sdW6FX8zhWFl6g96kZO555uHHzqc-O32x9XU2LZw/header.png?psid=1)
  ### la estructura del header es la siguiente (dentro del archivo main.tpl)
  ```html
    {include file="header.tpl"}
    {if ($popupwindow == 0)}
    {if ($EXPORT == 0)}

    <!-- las clases indicadas en el body es del adminlte -->
    <BODY class="hold-transition sidebar-mini">
    <!-- la clase wraper es el contenedor paratrabajar con el  adminlte  -->
    <div class="wrapper"  id="page-wrap">
        <div id="inside">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
           <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
        <a href="userinfo.php" class="nav-link">Inicio</a>
        </li>
      </ul>

       <!-- icono para salir de sesion -->
        <ul class="navbar-nav ml-auto">
         <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
             <a class="nav-link"  href='logout.php?logout=true'>
                <i class="text-danger fas fa-times-circle"></i>{php} echo gettext("LOGOUT");{/php}
             </a>
          </li>
        </ul>
    </nav>
  ```
  ## La Barra de Navegación
  ### la estructura de la barra de navegación es la siguiente
  ```html
     <!-- Inicio de la barra de navegación del AdminLte-->
     <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!--  Logo -->
     <a href="#" class="brand-link">
        <img src="AdminLTE/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">WiñaytelFono</span>
     </a>

     <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Inicio de un Item -->
          <li class="nav-item ">
            <a href="userinfo.php" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {php} echo gettext("ACCOUNT INFO");{/php}
              </p>
            </a>
          </li>
         <!-- fin de un item -->
        ...
        </ul>
       </nav>
     </div>

  ```
  ### los iconos los puedes encontrar en : https://fontawesome.com/icons?d=gallery

  #Estructura del cuerpo del AdminLTE
  ### para editar una seccion ingresamos al archivo php correspondiente para este ejemplo ingresaremos a userinfo.php
  ```php
     <?php
      include 'lib/customer.defines.php';
      include 'lib/customer.module.access.php';
      include 'lib/customer.smarty.php';
      include 'lib/epayment/includes/configure.php';
      include 'lib/epayment/includes/html_output.php';
      include './lib/epayment/includes/general.php';

        if (!has_rights(ACX_ACCESS)) {
           Header("HTTP/1.0 401 Unauthorized");
           Header("Location: PP_error.php?c=accessdenied");
           die();
        }
   
      ...
      smarty->display('main.tpl');
      ...
      include 'templates/default/user_info.php';
      $smarty->display('footer.tpl');


  ``` 
 ### aqui esta toda la logica , y  tambien puedes llamar a los archivos dentro de templates/default  para mostrar el diseño que deseas
 ### veamos dentro de el archivo templates/default/user_info.php la estructura del cuerpo del adminLTE
 ```php 
  <!-- clase que indica el cuerpo del AdminLTE -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">

      </div>
    </section>

  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        ...
       </div>
    </section>
  </div> 
 ```   
## EDITAR LOGIN CUSTOMER
### ingresar dentro de customer a : vim templates/default/index.tpl
![login](https://fogdca.bn.files.1drv.com/y4mWh2W6D5A7JE91lAv2_O6K0K6nDescdj4EjmPCpfJ2QTTmTQ1XjIyf9UZ71hVU0El_JKF63_Wgxb5mJlc6MpYTPut8OJ0wHC5v9Qd4soAVRywKNaQNb_lAY1UGfeeVsUv6bUyBsdAmzjRQeZVxt1awjFkvixY85vOyX8PylJnU94yuIRWcnYTKTMZ2edrnFbQ9JARVK4IaZpJDeuhmplnPA/login.png?psid=1)
### dentro de ese archivos esta todo el codigo (html , referencias a css y javascript ) para generar el login de la imagen
